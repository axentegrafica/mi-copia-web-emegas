<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="emegas.css" />
    <script src="main.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />
</head>


<body>
    <header>
        <h1>Rede EMEGA</h1>
        <p>Comunidade de apoio entre mulleres emprendedoras de Galiza</p>
    </header>
    <main>
        <section id="seccion-asesoramento">
            <img src="/imgs/infog-asesoramento.svg" alt="Asesoramento" />
            <h2>Asesoramento</h2>
            <p>O que eu sei + o que sabes ti</p>
            <ul>
                <li>
                    Maior probabilidade de <span class="text-black">resistencia</span>
                </li>
                <li><span class="text-black">Resiliencia</span></li>
                <li><span class="text-black">Supervivencia</span></li>
                <li><span class="text-black">Spstenibilidade</span></li>
                <li><span class="text-black">Éxito</span></li>
            </ul>
        </section>
        <section id="seccion-axuda">
            <img src="/imgs/infog-axuda.svg" alt="Axuda" />
            <h2>Axuda</h2>
            <p>Complementámonos:</p>
            <ul>
                <li>Xeramos traballo entre nós</li>
                <li>Tecemos as bases da nosa seguridade</li>
                <li>Cooperamos para acadar o equilibrio</li>
            </ul>
        </section>
        <section id="seccion-sororidade">
            <img src="/imgs/infog-sororidade.svg" alt="Sororidade" />
            <h2>Sororidade</h2>
            <p>Recontextualizamos:</p>
            <ul>
                <li>Emprendemento en equidade/li></li>
                <li>Prestamos atención á interseccionalidade</li>
                <li>Procuramos un presente seguro para nós</li>
            </ul>
        </section>
    </main>
    <footer>
        <p>Footer aquí</p>
    </footer>
</body>

</html>
